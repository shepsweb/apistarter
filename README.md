# Sirs\ApiStarter

A package to make the creation of api controller endpoints easy.

This package allows you to quickly create functional, boilerplate api controllers and fractal transformers so you can get your json enpoints up fast.

## Installation
* `composer require sirs/apistarter`
* paste `Sirs\ApiStarter\ApiStarterServiceProvider::class,` into config/app.php

## Usage

### Classes
#### ApiController

You give subclasses of this controller a model class and a Fractal transformer class and it provides resource methods that handle the generic use cases for index, store, show, update, destroy (if enabled).  Some features of #ApiController#:
* The index method accepts query parameters for for limiting, ordering, and filtering items.
	* limit
	* order_by
	* whitelisted filters
	* select (can be string or array: e.g. ?select=name or ?select[]=name&select[]=id) 
	* Example: `/api/participants?order_by=first_name&site_=1&select=` (where site_id has been defined in Api\UserController::$validFilters)
* The index and show methods accept #with# query parameter to request optional includes defined in your (custom) transformers. Example: `/api/participants/1?with=site` will include the participant's sites if the site relation has been defined as a include in your SiteTransformer.

#### GenericTransformer
This generic Fractal transformer class that utilizes the *TransformsAttributes* trait to transform your model's attributes (as retrieved by toArray()) into response data.  Generated custom transformers will also use the TransformsAttributes trait by default.

### Artisan Commands
You can quickly create ApiController subclasses and model-specific transformers with the include artisan commands:

#### make:api-controller
This command takes a model name (i.e. App\\User (make sure you escape the backslash)) and creates a controller in app/Http/Controllers/Api that inherits from Sirs\ApiStarter\Controllers\ApiController.  Now you can define your validFilters (if any) and/or override any resource methods you like.

###### Options
* `--transformer` generates a boilerplate transformer for you model(see below).
* `--destroyable` enables the destroy method in your controller (disabled by default).

##### Example
```
php artisan make:api-controler App\\User --transformer
```
will create:
```
app/Http/Controllers/Api/UserController.php
app/Http/Transformers/UserTransformer.php
```

#### make:api-transformer
This command takes a model name (i.e. App\\User (make sure you escape the backslash)) and creates a transformer in app/Http/Transformers.  This transformer uses a trait that will automatically include all non-hidden attributes of your model (anything that comes through in `$model->toArray()`).  See http://fractal.thephpleague.com/ for more info on Fractal and Transformers.

##### Example
```
php artisan make:api-transformer App\\Participant
```
will create:
```
app/Http/Transformers/ParticipantTransformer.php
```

NOTE: this is currently in dev so you'll have to update your minimum-stability in your composer.json file.
