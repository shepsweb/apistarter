<?php

namespace Sirs\ApiStarter;

use Illuminate\Support\ServiceProvider;
use Sirs\ApiStarter\Console\Commands\MakeApiController;
use Sirs\ApiStarter\Console\Commands\MakeTransformer;

class ApiStarterServiceProvider extends ServiceProvider {
	protected $defer = false;

	public function boot()
	{

	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
	    $this->commands([
	        MakeApiController::class,
	        MakeTransformer::class,
	    ]);
	}

}
