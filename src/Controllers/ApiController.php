<?php

namespace Sirs\ApiStarter\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item;
use Sirs\ApiStarter\BasicArraySerializer;

class ApiController extends Controller
{
    use DispatchesJobs, ValidatesRequests;

    /**
     * Fractal Manager
     *
     * @var League\Fractal\Manager
     **/
    protected $fractal;

    /**
     * Model class to use for this controller
     *
     * @var string
     **/
    protected $model;

    /**
     * Fractal Transformer to use when transforming model to response
     *
     * @var League\Fractal\TransformerAbstract
     **/
    protected $transformer;

    /**
     * Array of fields that can be searched
     * @var array
     */
    protected $validFilters = [];


    /**
     * whether to allower destory method to run
     *
     * @var boolean 
     **/
    protected $destroyable = false;


    /**
     * Instance of $model
     *
     * @var League\Fractal\TransformerAbstract
     **/
    protected $instance;

    public function __construct(){
        $this->fractal = new Manager();
        $this->fractal->setSerializer(new BasicArraySerializer());

    }

    protected function getFractalResponse($data, $transformerClass){
        return response()->json($this->fractal->createData($this->getFractalResource($data, $transformerClass))->toArray());
    }

    protected function getFractalResource($data, $transformerClass){
        if (is_array($data) || $data instanceOf Collection) {
            return new FractalCollection($data, new $transformerClass);
        }else{
            return new Item($data, new $transformerClass);
        }
    }

    protected function getModel()
    {
        if ($this->model) {
            return $this->model;
        }else{
            throw new \Exception('No model defined for ApiController');
        }
    }

    protected function getTransformer()
    {
        if ($this->transformer) {
            return $this->transformer;
        }else{
            throw new \Exception('No transformer defined for ApiController');
        }
    }

    public function index(Request $request)
    {   
        $collection = $this->getIndexData($request);

        return $this->getFractalResponse($collection, $this->transformer);
    }

    public function show($id)
    {
        if (request()->with) {
            $this->fractal->parseIncludes(request()->with);
        }
        $model = $this->model;
        return $this->getFractalResponse($model::find($id), $this->transformer);
    }

    public function store(Request $request)
    {
        $model = $this->model;
        $item = $model::create($request->all());

        return $this->getFractalResponse($item, $this->transformer);
    }

    public function update($id, Request $request)
    {
        $item = $this->model::find($id);
        $item->update($request->all());

        return $this->getFractalResponse($item, $this->transformer);
    }

    public function destroy($id)
    {
        if (! $this->destroyable) {
            return response(null, 404);
        }
        $item = $this->model::find($id);
        $item->delete();

        return ['deleted'=> true];
    }

    public function getIndexData(Request $request) 
    {
        return $this->getIndexQuery($request)->get();        
    }

    public function getIndexQuery(Request $request) 
    {
        $query = $this->model::query();

        // filter query
        foreach ($request->all() as $key => $value) {
            if (in_array($key, $this->validFilters)) {
                $query->where($key, $value);
            }
        }
        
        // Order query
        if ($request->order_by) {
            $parts = explode(':',$request->order_by);
            $field = $parts[0];
            $dir = (isset($parts[1])) ? $parts[1] : 'asc';
            
            $query->orderBy($field, $dir);
        }

        if ($request->limit) {
            $query->limit($request->limit);
        }

        if ($request->select) {
            $class = $this->model;
            $instance = new $class();
            $fields = [];
            if (is_array($request->select)) {
                foreach ($request->select as $field) {
                    if (\Schema::hasColumn($instance->getTable(), $field)) {
                        $fields[] = $field;
                    }
                }
            } else if (is_string($request->select)) {
                if (\Schema::hasColumn($instance->getTable(), $request->select)) {
                    $fields = $request->select;
                }
            }
            if (count($fields) > 0) {
                $query->select($fields);
            }
        }

        // TODO: look into pagination


        // handle requested includes
        if ($request->with) {
            $query->with(explode(',',$request->with));
            $this->fractal->parseIncludes($request->with);
        }
        return $query;
    }

}
