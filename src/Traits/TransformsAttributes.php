<?php
namespace Sirs\ApiStarter\Traits;

trait TransformsAttributes
{
  protected function transformAttributes($model)
  {
    $item = [];
    foreach( $model->toArray() as $attr => $val ){
      if( in_array($attr, ['deleted_at']) ) continue;
      $item[$attr] = $val;
    }

    $item['type'] = get_class($model);

    if ($model->pivot) {
      $item['pivot'] = $model->pivot;
    }
    return $item;
  }
}