<?php
namespace Sirs\ApiStarter\Transformers;

use League\Fractal\TransformerAbstract;
use Sirs\ApiStarter\Traits\TransformsAttributes;
use Sirs\ApiStarter\Transformers\GenericTransformer;

class GenericTransformer extends TransformerAbstract
{
  use TransformsAttributes;

  public function transform($model)
  {
    return $this->transformAttributes($model);
  }
}
