<?php

namespace Sirs\ApiStarter\Console\Commands;

use Illuminate\Console\Command;

class MakeTransformer extends Command
{
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:api-transformer 
    							{class : Model class to transform}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Fractal\Transformer in app/Http/Transformers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$name = preg_replace('/\\\?App\\\?/', '', $this->argument('class'));
    	$class = $this->argument('class');

    	$controllerString = file_get_contents(__DIR__.'/../../../stubs/Transformer.stub');
    	$controllerString = preg_replace('/\*NAME\*/', $name, $controllerString);
    	$controllerString = preg_replace('/\*MODEL\*/', $class, $controllerString);

    	$apiTransformerDir = app_path('Http/Transformers/');
    	$apiTransformerPath = $apiTransformerDir.$name.'Transformer.php';
		if (file_exists($apiTransformerPath)) {
			if (!$this->confirm($apiTransformerPath.' already exists. Do you want to overwrite it?')) {
                return;
            }
		}

    	if (! file_exists($apiTransformerDir)){
    		mkdir($apiTransformerDir);
    	}

    	file_put_contents($apiTransformerPath, $controllerString);
    	$this->info($apiTransformerPath.' created.  Don\'t forget to use it in your api controller!');
    }
}

