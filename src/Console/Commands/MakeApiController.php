<?php

namespace Sirs\ApiStarter\Console\Commands;

use Illuminate\Console\Command;

class MakeApiController extends Command
{
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:api-controller 
    							{class : Model class to create api resource for} 
    							{--transformer : Create a transformer for the model as well} 
    							{--destroyable : Allow destroy method to be called on api controller}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an ApiController in app/Http/Controllers/Api/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$name = preg_replace('/\\\?App\\\?/', '', $this->argument('class'));
    	$class = $this->argument('class');
    	$destroyable = $this->option('destroyable') ? 'true' : 'false';

        $transformer = 'GenericTransformer';
        if ($this->option('transformer')) {
            $transformerPath = app_path('Http/Transformers/'.$name.'Transformer.php');

            if (! file_exists($transformerPath)) {
                if ($this->confirm('The transformer doesn\'t exist.  Would you like to create it?')) {
                    \Artisan::call('make:api-transformer', ['class'=>$class]);
                    $transformer = '\App\Http\Transformers\\'.$name.'Transformer';
                }
            } else {
                $transformer = '\App\Http\Transformers\\'.$name.'Transformer';
            }
        }

    	$controllerString = file_get_contents(__DIR__.'/../../../stubs/ApiController.stub');
    	$controllerString = preg_replace('/\*NAME\*/', $name, $controllerString);
    	$controllerString = preg_replace('/\*MODEL\*/', $class, $controllerString);
    	$controllerString = preg_replace('/\*TRANSFORMER\*/', $transformer, $controllerString);
    	$controllerString = preg_replace('/\*DESTROYABLE\*/', $destroyable, $controllerString);


        $apiControllerDir = app_path('Http/Controllers/Api/');
        $apiControllerPath = $apiControllerDir.$name.'Controller.php';
        if (file_exists($apiControllerPath)) {
            $this->info($apiControllerPath.' already exists.');
            if (! $this->confirm('Do you want to overwrite it?')) {
                return;
            }
        }

    	if (! file_exists($apiControllerDir)){
    		mkdir($apiControllerDir);
    	}

    	file_put_contents($apiControllerPath, $controllerString);
    	$this->info($apiControllerPath.' created.  Don\'t forget to add a route!');
    }
}

